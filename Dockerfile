FROM php:7.2-fpm
MAINTAINER Sladjan Nikolic <sladjan.nikolic986@gmail.com>

COPY php.ini-development "$PHP_INI_DIR/php.ini"

RUN apt-get update -y && apt-get install -y libpng-dev libmagickwand-dev
RUN docker-php-ext-install gd
RUN docker-php-ext-install zip

RUN pecl install imagick && docker-php-ext-enable imagick
RUN pecl install xdebug-2.6.0 && pecl install mongodb && docker-php-ext-enable mongodb xdebug


