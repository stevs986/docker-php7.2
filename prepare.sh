#!/usr/bin/env bash

#sed -i 's/101/0/g' /usr/sbin/policy-rc.d
apt-get update
#apt-get install -y software-properties-common
#add-apt-repository ppa:ondrej/php -y && \
#apt-get update
#
apt-get install -y \
     php7.2-fpm \
     php7.2-curl \
     php7.2-gd \
     php7.2-mysql \
     php7.2-xsl \
     php-xdebug \
     php7.2-memcache \
     php7.2-redis \
     php7.2-imagick \
     php7.2-dev \
     pkg-config \
     php7.2-cli \
     php-pear \
     php7.2-common \
     php7.2-bz2\
     php7.2-mbstring\
     php7.2-zip \
     php7.2-sqlite3

/usr/bin/pecl install mongodb
#
#pear install PHP_CodeSniffer
#
echo "extension=mongodb.so" > /etc/php/7.2/mods-available/mongodb.ini
ln -s /etc/php/7.2/mods-available/mongodb.ini /etc/php/7.2/fpm/conf.d/20-mongodb.ini
ln -s /etc/php/7.2/mods-available/mongodb.ini /etc/php/7.2/cli/conf.d/20-mongodb.ini
#
sed -i 's/listen = \/run\/php\/php7.2-fpm.sock/listen = 9000/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i \
    -e '/^display_errors =/c\display_errors = On' \
    -e '/^display_startup_errors =/c\display_startup_errors = On' \
    -e '/^;date.timezone =/c\date.timezone = UTC' \
    -e '/^upload_max_filesize =/c\upload_max_filesize = 8m' \
    -e '/^error_reporting =/c\error_reporting = E_ALL' /etc/php/7.2/fpm/php.ini /etc/php/7.2/cli/php.ini
#
#apt-get install wkhtmltopdf
#
apt-get autoclean
apt-get clean
du -sh /var/cache/apt/archives
apt-get autoremove

